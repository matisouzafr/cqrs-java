package com.microservice.cqrs.controller;


import com.microservice.cqrs.service.ClientCommandService;
import com.microservice.cqrs.service.commands.CreateClientCommand;
import com.microservice.cqrs.service.commands.UpdateClientNameCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/clients")
public class ClientCommandController {
    @Autowired
    private ClientCommandService service;

    @PostMapping("/create")
    public void createClient(@RequestBody CreateClientCommand createClientCommand){
        this.service.handleCreateClientCommand(createClientCommand);
    }

    @PutMapping("/update/{id}")
    public void updateClient(@PathVariable("id") Long id, @RequestBody UpdateClientNameCommand updateClientNameCommand){
        updateClientNameCommand.setId(id);
        this.service.handleUpdateClientNameCommand(updateClientNameCommand);
    }


}
