package com.microservice.cqrs.controller;

import com.microservice.cqrs.persistence.replica.dto.ClientDto;
import com.microservice.cqrs.service.ClientQueryService;
import com.microservice.cqrs.service.queries.ClientsByName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/clients_query")
public class ClientQueryController {
    @Autowired
    private ClientQueryService service;

    @GetMapping("/clients/{name}")
    public List<ClientDto> getClients(@PathVariable("name") String name){
        System.out.println(name);
        return service.findAllCientsByName(new ClientsByName(name));
    }
}
