package com.microservice.cqrs.persistence.master;

import com.microservice.cqrs.persistence.models.Client;

import java.util.Optional;

public interface ClientWriteRepository {
    void save(Client client);
    void update(Client client);
    Optional<Client> findClientById(Long id);
}
