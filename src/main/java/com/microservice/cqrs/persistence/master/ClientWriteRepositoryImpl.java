package com.microservice.cqrs.persistence.master;

import com.microservice.cqrs.persistence.database.InMemoryDB;
import com.microservice.cqrs.persistence.models.Client;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class ClientWriteRepositoryImpl implements ClientWriteRepository{

    public InMemoryDB<Long, Client> masterDb;

    public ClientWriteRepositoryImpl(InMemoryDB<Long, Client> masterDb) {
        this.masterDb = masterDb;
    }

    @Override
    public void save(Client client) {
        this.masterDb.getStore().put(client.getId(), client);
    }

    @Override
    public void update(Client client) {
    }

    @Override
    public Optional<Client> findClientById(Long id) {
        return Optional.ofNullable(this.masterDb.getStore().get(id));
    }


}
