package com.microservice.cqrs.persistence.replica.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientDto {
    private String nombre;
    private String apellido;
}
