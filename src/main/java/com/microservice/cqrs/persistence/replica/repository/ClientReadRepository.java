package com.microservice.cqrs.persistence.replica.repository;

import com.microservice.cqrs.persistence.models.Client;
import com.microservice.cqrs.persistence.replica.dto.ClientDto;

import java.util.List;
import java.util.Optional;

public interface ClientReadRepository {
    Optional<Client> findById(String id);
    List<ClientDto> findByName(String name);
}
