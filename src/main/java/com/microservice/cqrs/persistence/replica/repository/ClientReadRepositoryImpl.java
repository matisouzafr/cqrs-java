package com.microservice.cqrs.persistence.replica.repository;

import com.microservice.cqrs.persistence.database.InMemoryDB;
import com.microservice.cqrs.persistence.models.Client;
import com.microservice.cqrs.persistence.replica.dto.ClientDto;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ClientReadRepositoryImpl implements ClientReadRepository{

    public InMemoryDB<Long, Client> replicaDb;

    public ClientReadRepositoryImpl(InMemoryDB<Long, Client> replicaDb) {
        this.replicaDb = replicaDb;
    }

    @Override
    public Optional<Client> findById(String id) {
        return Optional.empty();
    }

    @Override
    public List<ClientDto> findByName(String name) {
        System.out.println(this.replicaDb.getStore());

        return this.replicaDb
                .getStore()
                .values()
                .stream()
                .filter(client -> client.getNombre().equals(name))
                .map(client -> new ClientDto(client.getNombre(), client.getApellido()))
                .collect(Collectors.toList());
    }

}
