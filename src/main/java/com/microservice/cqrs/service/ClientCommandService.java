package com.microservice.cqrs.service;

import com.microservice.cqrs.service.commands.CreateClientCommand;
import com.microservice.cqrs.service.commands.UpdateClientNameCommand;

public interface ClientCommandService {
    void handleCreateClientCommand(CreateClientCommand createClientCommand);
    void handleUpdateClientNameCommand(UpdateClientNameCommand updateClientNameCommand);
}
