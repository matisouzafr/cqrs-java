package com.microservice.cqrs.service;

import com.microservice.cqrs.persistence.master.ClientWriteRepositoryImpl;
import com.microservice.cqrs.persistence.models.Client;
import com.microservice.cqrs.service.commands.CreateClientCommand;
import com.microservice.cqrs.service.commands.UpdateClientNameCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class ClientCommandServiceImpl implements ClientCommandService{
    Long id = 0L;
    @Autowired
    private ClientWriteRepositoryImpl repository;

    @Override
    public void handleCreateClientCommand(CreateClientCommand createClientCommand) {
        Client client =  new Client();
        client.setId(id++);
        client.setNombre(createClientCommand.getNombre());
        client.setApellido(createClientCommand.getApellido());
        this.repository.save(client);
    }

    @Override
    public void handleUpdateClientNameCommand(UpdateClientNameCommand updateClientNameCommand) {
        Optional<Client> client = repository.findClientById(updateClientNameCommand.getId());
        client.ifPresent(cliente -> cliente.setNombre(updateClientNameCommand.getNombre()));
    }
}
