package com.microservice.cqrs.service;

import com.microservice.cqrs.persistence.replica.dto.ClientDto;
import com.microservice.cqrs.service.queries.ClientsByName;

import java.util.List;

public interface ClientQueryService {
    List<ClientDto> findAllCientsByName(ClientsByName clientsByName);
}
