package com.microservice.cqrs.service;

import com.microservice.cqrs.persistence.replica.dto.ClientDto;
import com.microservice.cqrs.persistence.replica.repository.ClientReadRepositoryImpl;
import com.microservice.cqrs.service.queries.ClientsByName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ClientQueryServiceImpl implements ClientQueryService{

    @Autowired
    private ClientReadRepositoryImpl repository;

    @Override
    public List<ClientDto> findAllCientsByName(ClientsByName clientsByName) {
        return repository.findByName(clientsByName.getNombre());
    }
}
