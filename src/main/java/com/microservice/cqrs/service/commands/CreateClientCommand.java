package com.microservice.cqrs.service.commands;

import lombok.Data;

@Data
public class CreateClientCommand {
    private String nombre;
    private String apellido;
}
