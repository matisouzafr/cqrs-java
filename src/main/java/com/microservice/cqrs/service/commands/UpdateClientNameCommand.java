package com.microservice.cqrs.service.commands;

import lombok.Data;

@Data
public class UpdateClientNameCommand {
    private Long id;
    private String nombre;
}
