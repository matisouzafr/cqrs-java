package com.microservice.cqrs.service.queries;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientsByName {
    private String nombre;
}
